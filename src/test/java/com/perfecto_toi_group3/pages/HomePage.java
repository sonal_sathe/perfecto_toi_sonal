/**
 * 
 */
package com.perfecto_toi_group3.pages;

import java.util.HashMap;
import java.util.Map;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

/**
 * @author Megha.Garg
 */
public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {

	}
	public void launchPage() {
		//driver.get("/");
		Map<String, Object> params = new HashMap<>();
		   
		params.put("identifier", "driver.capabilities.appPackage");
		driver.executeScript("mobile:application:open", params);
	}
}
